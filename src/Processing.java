import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

public class Processing {
    private List<MonitoredData> monitoredData;

   public Processing(){
       monitoredData = new ArrayList<>();
       readFromFile();
   }

    public List<MonitoredData> getMonitoredData() {
        return monitoredData;
    }

    public void readFromFile(){
        try(BufferedReader br = new BufferedReader(new FileReader("Activities.txt"))) {

            String line = br.readLine();
            DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            while (line != null) {
                String[] linie = line.split("\\s+");

                LocalDateTime start = LocalDateTime.from(f.parse(linie[0] + " " + linie[1]));
                LocalDateTime end = LocalDateTime.from(f.parse(linie[2] + " " + linie[3]));
                String activitate = linie[4];
                MonitoredData data = new MonitoredData(start, end, activitate);


                line = br.readLine();
                if(linie == null){
                    break;
                }
                monitoredData.add(data);
            }
        }catch(Exception e){
            e.printStackTrace();

        }
    }

    public void scriereFrecventa(){
        Map<String, Long> countActivitati =
                monitoredData.stream().collect(Collectors.groupingBy(e -> e.getActivityLabel(), Collectors.counting()));
        try {
            Files.write(Paths.get("Frecventa"), () -> countActivitati.entrySet().stream()
                    .<CharSequence>map(e -> e.getKey() + " " + e.getValue())
                    .iterator());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void scrieFiltru(){
        Map<String,Long> mapa = new HashMap<>();
        for(MonitoredData m:monitoredData){
            if(mapa.containsKey(m.getActivityLabel())){
                mapa.put(m.getActivityLabel(), mapa.get(m.getActivityLabel()) +  ChronoUnit.SECONDS.between(m.getStartTime(), m.getEndTime()));
            }
            else{
                mapa.put(m.getActivityLabel(), ChronoUnit.SECONDS.between(m.getStartTime(), m.getEndTime()));
            }

        }
        mapa = mapa.entrySet().stream().filter(e -> e.getValue() > 36000).collect(Collectors.toMap(e->e.getKey(),e->e.getValue()));;
        Map<String,Long> mapa1=mapa;
        try {
            Files.write(Paths.get("Filtrare"), () -> mapa1.entrySet().stream()
                    .<CharSequence>map(e -> e.getKey() + " " + e.getValue()/3600+" ore")
                    .iterator());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void scrieFrecventaZile(){
        Map<Integer,Map<String,Long>> mapa = new HashMap<>();
        for(MonitoredData m:monitoredData){
            Map<String, Long> zi =
                    monitoredData.stream().filter(e -> e.getStartTime().getDayOfMonth() == m.getStartTime().getDayOfMonth()).collect(Collectors.groupingBy(e -> e.getActivityLabel(), Collectors.counting()));
            if(!mapa.containsKey(m.getStartTime().getDayOfMonth())){
                mapa.put(m.getStartTime().getDayOfMonth(), zi);
            }
        }

        try {
            Files.write(Paths.get("FrecventaZile"), () -> mapa.entrySet().stream()
                    .<CharSequence>map(e -> e.getKey() + " " + e.getValue())
                    .iterator());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
